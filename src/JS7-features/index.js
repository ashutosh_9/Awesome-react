import {name, arrFn1, arrFn2, arrFn3, paramPass, arrFn4, arrFn5, arr} from './arrow-func.js';
export let myName = name;
export let fnWithoutParam = arrFn1();
export let fnShort = arrFn2();
export let fnParamwithBraces = arrFn3(paramPass);
export let fnParamWithoutBraces = arrFn4(paramPass);
export let fnSpreadParamArr = arrFn5(arr);

console.log('Hello ES6');
