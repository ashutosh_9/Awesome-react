export let name = "ES6 Features -- Arrow Function";

export let arrFn1 = () => {
  return 'arrow function without parameter';
}

export let arrFn2 = () => 'arrow function shorter syntax in single line';

export let paramPass = 'Arrow function Single Parameter with braces';

export let arrFn3 = (paramPass) => {
  return paramPass;
}

export let arrFn4 = paramPass => {
  return paramPass + ' removal';
}

export let arr = [1,4,5,7];

export let arrFn5 = (...arr) => {
  return arr;
}
